const mongoose = require('mongoose')
const Schema = mongoose.Schema

let MatchSchema = new Schema({
  startTime: {
    type: Date,
    default: Date.now(),
    required: true
  },
  // Survivor
  escaped: Boolean,
  numberEscaped: Number,
  tunneled: Boolean,
  hookCamped: Boolean,
  mori: Boolean,
  // mainSurvivorID: String,
  mainSurvivorID: { type: Schema.Types.ObjectId, ref: 'DSSurvivor' },
  teamSurvivorIDs: [{type: Schema.Types.ObjectId, ref: 'DSSurvivor'}],
  // Killer
  numberSacrifices: Number,
  numberDisconnect: Number,
  toxicSurvivors: Boolean,
  maybeSWF: Boolean,
  killerID: String,
  // Misc
  matchNotes: String,
  // Location
  realm: String,
  map: String,
  // K or S game flag
  isSurvivor: Boolean
})

module.exports = mongoose.model('DSMatch', MatchSchema)