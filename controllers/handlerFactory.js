const appError = require('../utils/appError')

const catchAsync = fn => {
  return (req, res, next) => {
    fn(req, res, next).catch(err => next(err))
  }
}

// execPopulate() removed from Mongoose 6+
exports.getOne = (Model, popOptions) =>
  catchAsync(async(req, res, next) => {
    let query = await Model.findById(req.params.id)
    if (popOptions) query = query.populate(popOptions)
    const doc = await query

    if (!doc) return next(new appError('No document found with that ID.', 404))

    res.status(200).json({
      status: 'success',
      data: {
        data: doc
      }
    })
  })

exports.getAll = Model =>
  catchAsync(async(req, res, next) => {
    // TODO write up a better explanation here, nested GETs
    // let filter = {}
    // if (req.params.caseId) filter = { case: req.params.caseId }

    // const features = new apiFeatures(Model.find(filter), req.query)
    //   .filter()
    //   .sort()
    //   .limitFields()
    //   .paginate()

    const doc = await Model.find()

    res.status(200).json({
      status: 'success',
      results: doc.length,
      data: {
        data: doc
      }
    })
  })