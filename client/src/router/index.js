import Vue from "vue";
import Router from "vue-router";
import AvatarGrid from "@/views/AvatarGrid.vue";
import KillerGrid from '@/views/KillerGrid'
import EndMatch from '@/views/EndMatch'
import LoginUser from '@/views/LoginUser'

Vue.use(Router)

const routes = [
  {
    path: '/login',
    name: 'LoginUser',
    component: LoginUser
  },
  {
    path: "/",
    name: "AvatarGrid",
    component: AvatarGrid,
    meta: { requiresAuth: true }
  },
  {
    path: "/killer",
    name: "KillerGrid",
    component: KillerGrid,
    meta: { requiresAuth: true }
  },
  {
    path: "/end-match",
    name: "EndMatch",
    component: EndMatch,
    meta: { requiresAuth: true }
  }
]

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  // console.log(`localStorage.getItem(user) : ${localStorage.getItem('user')}`)
  const loggedIn = localStorage.getItem('user')

  if (to.matched.some(record => record.meta.requiresAuth) && !loggedIn) {
    next({ name: 'LoginUser'} )
  }
  next()
})

export default router



