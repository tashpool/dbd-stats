const DSSurvivor = require('../models/DSSurvivor')
const handlerFactory = require('../controllers/handlerFactory')

exports.dssurvivor_list = handlerFactory.getAll(DSSurvivor)
exports.dssurvivor_detail = handlerFactory.getOne(DSSurvivor)
