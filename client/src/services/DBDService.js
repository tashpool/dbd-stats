import axios from 'axios'
import store from '../store/index'

export const axios_instance = axios.create({
  baseURL: '/api',
  // baseURL: 'http://localhost:8081/api',
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

axios_instance.interceptors.request.use((config) => {
  console.log('DBDService: Checking for logged in user...')
  if (store.getters.loggedIn) {
    console.log(`Found token, setting req.headers.auth...`)
    config.headers.authorization = `Bearer ${store.state.user.token}`
    // axios_instance.defaults.headers.common['Authorization'] = `Bearer ${store.state.user.token}`
  } else {
    console.log(`DBDService: No Logged In User found!`)
  }

  return config
})

// -=-=-=-=-=-= User
export function loginUser(credentials) {
  return axios_instance.post('/users/login', credentials)
}

export function signupUser( userData ) {
  return axios_instance.post('/users/signup', userData)
}

// Match Data
export function postMatch(matchObject) {
  return axios_instance.post('/match', matchObject)
}

// Get User IDs from mongo DB
export function getSurvivors() {
  return axios_instance.get('/survivor')
}