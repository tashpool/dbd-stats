if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config()
}

const app = require('./app')
const path = require('path')

// --- Mongo Connection Setup
const mongoose = require('mongoose')
const mongoConnectString = process.env.mongoDB.replace(
  '<PASSWORD>',
  process.env.mongoPassword
)

mongoose.connect(mongoConnectString)
  .catch(error => console.error(error))

let db = mongoose.connection
db.once('open', () => {
  console.log('MongoDB DB connection established successfully.')
})
db.on('error', console.error.bind(console, 'MongoDB connection error: '))
// --- End Mongo Connection Setup

// --- Server
const port = Number(process.env.PORT) || 8081

const server = app.listen(port, () => {
  console.log(`DBD Stats starting on port ${port}.`);
})

