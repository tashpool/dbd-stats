const User = require('./../models/DSUser')
const catchAsync = require('./../utils/catchAsync')
const appError = require('./../utils/appError')
const jwt = require('jsonwebtoken')
const {promisify} = require('util')

/**
 * Upon token and user verification, req.user is set with user db entry
 */
exports.protect = catchAsync( async(req, res, next) => {
  // 1. Check for a token
  let token
  if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
    // token was double-quoted for some reason?
    // console.log(`Req.headers.auth: ${req.headers.authorization}`)
    token = req.headers.authorization.split(' ')[1]
  }

  if (!token) { return next(new appError('Please log in to get access'), 401) }

  // 2. Decode the token given
  const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET)

  // 3. Does the user exist?
  // Has user changed their password, or their account was deleted
  const currentUser = await User.findById(decoded.id)
  if (!currentUser) {
    return next(new appError('The user for this token no longer exists.'), 401)
  }

  // 4. Did the user password change AFTER the token was issued?
  if (currentUser.changedPasswordAfter(decoded.iat)) {
    return next(new appError('User recently changed password, please log in again.'), 401)
  }

  // Set decoded user info to req object and grant access to protected route
  req.user = currentUser
  next()
})

exports.signup = catchAsync(async(req, res, next) => {
  const newUser = await User.create({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
    passwordConfirm: req.body.passwordConfirm
  })

  createSendToken(newUser, 201, res)
})

exports.login = catchAsync( async(req, res, next) => {
  const { email, password } = req.body

  // 1. Email & Pass are required
  if (!email || !password) {
    return next(new appError('Email and password are required.', 400))
  }

  // 2. Find user by email
  const user = await User.findOne({ email }).select('+password')

  // 3a. User exists
  // 3b. Password is validated
  if(!user || !(await user.correctPassword(password, user.password))) {
    return next(new appError('Incorrect email or password', 401))
  }

  createSendToken(user, 200, res)
})

const signToken = id => {
  return jwt.sign({ id },
    process.env.JWT_SECRET,
    { expiresIn: process.env.JWT_EXPIRES_IN})
}

/*
https://expressjs.com/en/api.html#res.cookie
 */
const createSendToken = (user, statusCode, res) => {
  const token = signToken(user._id)
  const cookieOptions = {
    expires: new Date(Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000),
    httpOnly: true
  }
  if (process.env.NODE_ENV === 'production') cookieOptions.secure = true

  res.cookie('jwt', token, cookieOptions)

  // Keep the password out of the response data, the JWT will identify the user
  user.password = undefined

  res.status(statusCode).json({
    status: 'success',
    token,
    data: {
      user
    }
  })
}