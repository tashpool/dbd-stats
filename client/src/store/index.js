import Vue from 'vue'
import Vuex from 'vuex'
import * as snackbar from './modules/snackbar'
import * as DBDService from '../services/DBDService'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    snackbar
  },
  state: {
    user: null,
    selectedPlayers: [],
    selectedKiller: null
  },
  mutations: {
    SET_USER_DATA(state, userData) {
      state.user = userData
      localStorage.setItem('user', JSON.stringify(userData))
    },
    CLEAR_USER_DATA() {
      localStorage.removeItem('user')
      location.reload()
    },
    SET_USER_STATE(state) {
      state.user = JSON.parse(localStorage.getItem('user'))
    },
    SET_PLAYER (state, payload) {
      state.selectedPlayers.push(payload.player)
    },
    CLEAR_PLAYER (state) {
      state.selectedPlayers.pop()
    },
    CLEAR_ALL_PLAYERS (state) {
      state.selectedPlayers = []
    },
    SET_KILLER (state, payload) {
      state.selectedKiller = payload.killer
    },
    CLEAR_KILLER (state) {
      state.selectedKiller = null
    }
  },
  actions: {
    signup({ commit }, credentials) {
      return DBDService.signupUser(credentials)
        .then(({ data }) => {
          commit('SET_USER_DATA', data)
        })
        .catch(err => {
          return err.response
        })
    },
    login({ commit }, credentials) {
      return DBDService.loginUser(credentials)
        .then(({data}) => {
          commit('SET_USER_DATA', data)
        })
        .catch(err => {
          console.log('Axios Error: ', err.response)
          return err.response
        })
    },
    logout({ commit }) {
      commit('CLEAR_USER_DATA')
    },
    checkState({ commit, state }) {
      // We have user info in web storage, but not in the vuex state
      try {
        if (localStorage.getItem('user') && !state.user) {
          commit('SET_USER_STATE')
        }
      } catch(err) {
        console.log('User State Error: ', err.response)
        return err.response
      }
    },
    setPlayer({commit, state}, playerID) {
      if (state.selectedPlayers.length < 4) {
        commit('SET_PLAYER', playerID)
      }
    },
    clearPlayer({commit}) {
      commit('CLEAR_PLAYER')
    },
    clearAllPlayers({commit}) {
      commit('CLEAR_ALL_PLAYERS')
    },
    setKiller({commit}, killerID) {
      commit('SET_KILLER', killerID)
    },
    clearKiller({commit}) {
      commit('CLEAR_KILLER')
    },
    resetAllState({commit}) {
      commit('CLEAR_ALL_PLAYERS')
      commit('CLEAR_KILLER')
    }
  },
  getters: {
    loggedIn(state) {
      return !!state.user
    },
    getPlayers: state => {
      return state.selectedPlayers
    },
    getNumberOfPlayers: state => {
      return state.selectedPlayers.length
    },
    getLastPlayer: state => {
      if (state.selectedPlayers.length > 0) {
        return state.selectedPlayers[state.selectedPlayers.length-1]
      }
      return null
    },
    getKiller: state => {
      return state.selectedKiller
    }
  }
})
