const express = require('express')
const survivorController = require('../controllers/DSSurvivorController')

const router = express.Router()

router.route('/')
  .get(survivorController.dssurvivor_list)

router.route('/:id')
  .get(survivorController.dssurvivor_detail)

module.exports = router