const DSKiller = require('../models/DSKiller')
const handlerFactory = require('../controllers/handlerFactory')

exports.dskiller_list = handlerFactory.getAll(DSKiller)