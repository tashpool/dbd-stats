const express = require('express')
const matchController = require('./../controllers/DSMatchController')
const authController = require('./../controllers/authController')

const router = express.Router()

router.route('/')
  .get(authController.protect,
    matchController.dsmatch_list)
  .post(authController.protect,
    matchController.dsmatch_create)

module.exports = router