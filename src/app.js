const express = require('express')
const app = express()
const path = require('path')
const AppError = require('./../utils/appError')
const globalErrorHandler = require('./../controllers/errorController')
const serveStatic = require('serve-static')

// --- Routes
const userRouter = require('../routes/userRouter')
const matchRouter = require('../routes/matchRouter')
const killerRouter = require('../routes/killerRouter')
const survivorRouter = require('../routes/survivorRouter')

app.use(express.json())

// FOR TESTING ONLY - Add Access Control Allow Origin headers
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*")
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  )
  next()
})

// Serve Images
app.use(express.static(path.join(__dirname, '../public')))

// ---- Vue Front End Integration
app.use(serveStatic(path.join(__dirname, '../client/dist/')))

// this * route is to serve project on different page routes except root `/`
app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '../client/dist/index.html'))
})

app.get('/', (req, res) => {
  res.send('DBD Stats Home page')
})

// --- Routers ---
app.use('/api/users', userRouter)
app.use('/api/match', matchRouter)
app.use('/api/killer', killerRouter)
app.use('/api/survivor', survivorRouter)

// Catch all at the end for all routes
app.all('*', (req, res, next) => {
  next(new AppError(`Can't find ${req.originalUrl} on server.`, 404))
})

// if you throw an error with next(), this is where we go (4 params)
app.use(globalErrorHandler)

module.exports = app
