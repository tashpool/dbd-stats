const express = require('express')
const killerController = require('../controllers/DSKillerController')

const router = express.Router()

router.route('/')
  .get(killerController.dskiller_list)

module.exports = router