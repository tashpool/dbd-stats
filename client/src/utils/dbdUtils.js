export function capitalizeWord(incomingWord) {
  return incomingWord.charAt(0).toUpperCase() + incomingWord.slice(1)
}