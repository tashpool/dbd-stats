const DSMatch = require('../models/DSMatch')
const handlerFactory = require('../controllers/handlerFactory')
const { body, validationResult } = require('express-validator')

exports.dsmatch_list = handlerFactory.getAll(DSMatch)

exports.dsmatch_create = [
  body('matchNotes').isLength({max: 500})
    .trim().withMessage('Match notes max length is 500 characters.'),
  body('mainSurvivorID').exists()
    .trim().withMessage('Main survivor ID required.'),
  // body('startTime').exists().toDate(),

  async function(req, res, next) {
    console.log(`\nEntered DSMatch Create`)
    console.log(`Req.body: ${JSON.stringify(req.body)}\n`)

    let defaultStartTime = req.body.startTime
    if (defaultStartTime === null) {
      defaultStartTime = new Date(Date.now())
    }

    const validationErrorChecks = validationResult(req)

    if (!validationErrorChecks.isEmpty()) {
      console.log(`Match create validation failed`)
      const err = new Error(JSON.stringify(validationErrorChecks))
      err.status = 'error'
      err.statusCode = 422

      return next(err)
    }

    // check for array inputs
    if (req.body.teamSurvivorIDs && !Array.isArray(req.body.teamSurvivorIDs)) {
      const err = new Error('Team IDs must be an array of Object IDs.')
      err.status = 'error'
      err.statusCode = 422

      return next(err)
    }

    let newDSMatch = new DSMatch(
      {
        startTime: defaultStartTime,
        escaped: req.body.escaped,
        numberEscaped: req.body.numberEscaped,
        tunneled: req.body.tunneled,
        hookCamped: req.body.hookCamped,
        mori: req.body.mori,
        mainSurvivorID: req.body.mainSurvivorID,
        teamSurvivorIDs: req.body.teamSurvivorIDs,
        numberSacrifices: req.body.numberSacrifices,
        numberDisconnect: req.body.numberDisconnect,
        toxicSurvivors: req.body.toxicSurvivors,
        maybeSWF: req.body.maybeSWF,
        killerID: req.body.killerID,
        matchNotes: req.body.matchNotes,
        realm: req.body.realm,
        map: req.body.map,
        isSurvivor: req.body.isSurvivor
      }
    )

    try {
      let savedMatch = await newDSMatch.save()
      res.status(200).json({
        status: "success",
        msg: "Match data added to system.",
        data: {
          savedMatch
        }
      })
    } catch(tryError) {
      tryError.status = 'error'
      tryError.statusCode = 500
      return next(tryError)
    }
  }
]