let mongoose = require('mongoose')
let Schema = mongoose.Schema

let SurvivorSchema = new Schema({
  id: { type: String },
  name: { type: String },
  fullName: { type: String }
})

module.exports = mongoose.model('DSSurvivor', SurvivorSchema)