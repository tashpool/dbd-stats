let mongoose = require('mongoose')
let Schema = mongoose.Schema

let KillerSchema = new Schema({
  id: { type: String },
  name: { type: String },
  fullName: { type: String }
})

module.exports = mongoose.model('DSKiller', KillerSchema)